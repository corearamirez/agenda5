﻿using Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebMVC.Tests.Controllers
{

    [TestClass]
    public class CalculatorTest
    {
        
        [TestInitialize]
        public void OnTestInitialize()
        {
            _SystemUnderTest = null;
        }

        private Calculator _SystemUnderTest;

        public Calculator SystemUnderTest
        {
            get
            {
                if(_SystemUnderTest == null)
                {
                    _SystemUnderTest = new Calculator();
                }
                return _SystemUnderTest;
            }


        }



 
        [TestMethod]
        public void Add()
        {

            //Arrange (Organizar)
            int value1 = 2;
            int value2 = 3;
            int expected = 5;

            //Act (Actuar)


            int actual = SystemUnderTest.Add(value1,value2);

            //Assert (Afirmar)
            Assert.AreEqual<int>(expected, actual, "Error, valores no coinciden");

        }


        [TestMethod]
        public void Subtract()
        {

            //Arrange (Organizar)
            int value1 = 20;
            int value2 = 10;
            int expected = 10;

            //Act (Actuar)


            int actual = SystemUnderTest.Subtract(value1, value2);

            //Assert (Afirmar)
            Assert.AreEqual<int>(expected, actual, "Error, valores no coinciden");

        }

        [TestMethod]
        public void Multiply()
        {

            //Arrange (Organizar)
            int value1 = 20;
            int value2 = 5;
            int expected = 100;

            //Act (Actuar)


            int actual = SystemUnderTest.Multiply(value1, value2);

            //Assert (Afirmar)
            Assert.AreEqual<int>(expected, actual, "Error, valores no coinciden");

        }


        [TestMethod]

        public void Divide()
        {

            //Arrange (Organizar)
            int value1 = 100;
            int value2 = 25;
            int expected = 4;

            //Act (Actuar)


            int actual = SystemUnderTest.Divide(value1, value2);

            //Assert (Afirmar)
            Assert.AreEqual<int>(expected, actual, "Error, valores no coinciden");

        }






    }
}
